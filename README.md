# Welcome to Tutorial PRICES 2021
Precise Requirement Changes Integrated System (**PRICES**) is a web-based product line generator
based on model-driven software engineering and delta-oriented programming.
**PRICES** consists of tools and frameworks to develop a web-based product line application.
This tutorial explains how to run **PRICES** in your machine using a case study, an adaptive
system for charity organization (AISCO).

Please check this [WIKI](https://gitlab.com/RSE-Lab-Fasilkom-UI/training2021/splc-training-2021/-/wikis/home) page to follow the tutorial. 
